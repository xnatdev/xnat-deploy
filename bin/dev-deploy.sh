#!/bin/bash
#
# dev-deploy.sh
# XNAT http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# This script deploys the new build to the Tomcat 7 webapps folder then tails the
# catalina.out log until the application has deployed or until a failure occurs.
#

ORIGIN=`dirname $0`
source ${ORIGIN}/macros.sh

WAR=$(find ${HOME} -mindepth 1 -maxdepth 1 -type f -name "xnat-web.war" -printf "%f\n" | sort -r | head -n 1)

if [[ -z "${WAR// }" ]]; then
    error -10 No XNAT war file found in the home folder ${HOME}, nothing to deploy so exiting.
fi

cp -v ${WAR} /usr/share/tomcat/webapps/ROOT.war

waitForFile /usr/share/tomcat/webapps/ROOT.war
monitorTomcatStatus 180 verbose "INFO: Deployment of web application archive .*ROOT.war"

STATUS=$?
if [[ ${STATUS} == 0 ]]; then
    echo The application was started successfully. Cleaning up artifacts...;
    rm ${WAR}
    exit 0;
else
    FAILED=${WAR}.failed-$(date +%Y%m%d%H%M%S)
    mv ${WAR} ${FAILED}
    echo The application does not appear to have started properly. Status code: ${STATUS}
    echo The artifact that failed to deploy has been left for forensics purposes: ${FAILED}
    echo The last lines in the log are:; tail -n 40 /var/log/tomcat/catalina.$(date +%Y-%m-%d).out;
fi

exit ${STATUS}
