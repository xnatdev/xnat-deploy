#!/bin/bash
#
# commitTagPushRepos.sh
# XNAT http://www.xnat.org
# Copyright (c) 2017, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# This script is used to go to all subdirectories of the current directory, 
# commit any changes, add a version tag, and push those changes. You should
# change v to whatever you want the version tag to be.
#
v=1.7.4 ; for i in `ls -d */`; do echo "--- Committing, tagging with $v, and pushing $i" ; cd $i ; git add . ; git commit -m "Updating to $v for final release" ; git push ; git tag $v ; git push --tags ; cd .. ; echo "--- Finished $i" ; done
