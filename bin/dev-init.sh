#!/bin/bash

ORIGIN=`dirname $0`
source ${ORIGIN}/macros.sh

shopt -s nocasematch

if [[ -z ${1} ]]; then
    echo You must specify at least one argument, the address of the server being initialized. You can also specify, in order, title, email, initializing user and password.
    exit -1
fi

ADDRESS=${1}
[[ -n ${2} ]] && { TITLE=${2}; } || { TITLE=XNAT; }
[[ -n ${3} ]] && { EMAIL=${3}; } || { EMAIL=xnatselenium@gmail.com; }
[[ -n ${4} ]] && { USER=${4}; } || { USER=admin; }
[[ -n ${5} ]] && { PASSWORD=${5}; } || { PASSWORD=admin; }

SERVER=$(echo ${ADDRESS} | sed 's#http[s]*://##')
WORK=${HOME}/.deploy/${SERVER}
[[ -d ${WORK} ]] || { mkdir -p ${WORK}; }

authenticate

XNAT_HOME=$(eval echo ~${XNAT_USER})
DATA_ROOT=$(dirname ${XNAT_HOME})

echo Running the command: curl --config ${CONFIG} --data "{\"siteId\":\"${TITLE}\",\"siteUrl\":\"${ADDRESS}\",\"adminEmail\":\"${EMAIL}\"}" ${ADDRESS}/xapi/siteConfig
echo Return status was: $(curl --config ${CONFIG} --data "{\"siteId\":\"${TITLE}\",\"siteUrl\":\"${ADDRESS}\",\"adminEmail\":\"${EMAIL}\"}" ${ADDRESS}/xapi/siteConfig)

echo Running the command: curl --config ${CONFIG} --data "{\"archivePath\":\"${DATA_ROOT}/archive\",\"prearchivePath\":\"${DATA_ROOT}/prearchive\",\"cachePath\":\"${DATA_ROOT}/cache\"}" ${ADDRESS}/xapi/siteConfig
echo Return status was: $(curl --config ${CONFIG} --data "{\"archivePath\":\"${DATA_ROOT}/archive\",\"prearchivePath\":\"${DATA_ROOT}/prearchive\",\"cachePath\":\"${DATA_ROOT}/cache\"}" ${ADDRESS}/xapi/siteConfig)

echo Running the command: curl --config ${CONFIG} --data "{\"buildPath\":\"${DATA_ROOT}/build\",\"ftpPath\":\"${DATA_ROOT}/ftp\",\"pipelinePath\":\"${DATA_ROOT}/pipeline\"}" ${ADDRESS}/xapi/siteConfig
echo Return status was: $(curl --config ${CONFIG} --data "{\"buildPath\":\"${DATA_ROOT}/build\",\"ftpPath\":\"${DATA_ROOT}/ftp\",\"pipelinePath\":\"${DATA_ROOT}/pipeline\"}" ${ADDRESS}/xapi/siteConfig)

echo Running the command: curl --config ${CONFIG} --data "{\"host\":\"mail.nrg.wustl.edu\",\"port\":25,\"username\":\"\",\"password\":\"\",\"protocol\":\"smtp\"}" ${ADDRESS}/xapi/notifications
echo Return status was: $(curl --config ${CONFIG} --data "{\"host\":\"mail.nrg.wustl.edu\",\"port\":25,\"username\":\"\",\"password\":\"\",\"protocol\":\"smtp\"}" ${ADDRESS}/xapi/notifications)

echo Running the command: curl --config ${CONFIG} --data "{\"mail.smtp.auth\":\"\",\"mail.smtp.starttls.enable\":\"\",\"mail.smtp.ssl.trust\":\"\"}" ${ADDRESS}/xapi/notifications
echo Return status was: $(curl --config ${CONFIG} --data "{\"mail.smtp.auth\":\"\",\"mail.smtp.starttls.enable\":\"\",\"mail.smtp.ssl.trust\":\"\"}" ${ADDRESS}/xapi/notifications)

echo Running the command: curl --config ${CONFIG} --data "{\"requireLogin\":true,\"userRegistration\":false,\"enableCsrfToken\":true}" ${ADDRESS}/xapi/siteConfig
echo Return status was: $(curl --config ${CONFIG} --data "{\"requireLogin\":true,\"userRegistration\":false,\"enableCsrfToken\":true}" ${ADDRESS}/xapi/siteConfig)

echo Running the command: curl --config ${CONFIG} --data "{\"initialized\":true}" ${ADDRESS}/xapi/siteConfig
echo Return status was: $(curl --config ${CONFIG} --data "{\"initialized\":true}" ${ADDRESS}/xapi/siteConfig)

echo Terminating all sessions: curl --config ${CONFIG} --request DELETE ${ADDRESS}/xapi/users/active/${USER}
echo Return status was: $(curl --config ${CONFIG} --request DELETE ${ADDRESS}/xapi/users/active/${USER})
