#!/bin/bash
#
# macros.sh
# XNAT http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# Common functions used for the deployment scripts.
#

# Shows the configured usage message, exits with the error code (first parameter)
# then displays all remaining parameters as an error message.
showHelp() {
    [[ -n ${1} ]] && { CODE=${1}; shift; } || { CODE=0; }
    [[ -n ${1} ]] && { echo Error: $@; }
    echo ${USAGE}
    exit ${CODE}
}

# Displays a message only if the flag DEBUG is true.
debug() {
    [[ ${DEBUG} == true ]] && { echo $@; }
}

# Exits with the error code (first parameter) and displays all remaining parameters
# as the error message.
error() {
    [[ -n ${1} ]] && { CODE=${1}; shift; } || { CODE=0; }
    [[ -n ${1} ]] && { echo Error: $@; } || { echo Error: unknown.; }
    exit ${CODE}
}

# Creates the cookie file containing validated JSESSIONID.
authenticate() {
    CONFIG=${WORK}/config.txt
    COOKIE=${WORK}/cookie.txt
    echo --header \"Host: ${SERVER}\" > ${CONFIG}
    echo --header \"Accept: */*\" >> ${CONFIG}
    echo --header \"Origin: ${ADDRESS}\" >> ${CONFIG}
    echo --header \"Content-Type: application/json\" >> ${CONFIG}
    echo --header \"Referer: ${ADDRESS}/setup/\" >> ${CONFIG}
    echo --output /dev/null >> ${CONFIG}
    echo --write-out "%{http_code}" >> ${CONFIG}
    echo --silent >> ${CONFIG}

    if [[ -e ${COOKIE} ]]; then
        STATUS=$(curl -s --cookie ${COOKIE} --config ${CONFIG} -o /dev/null -w "%{http_code}" ${ADDRESS}/data/projects)
    else
        STATUS=401
    fi

    if [[ ${STATUS} == 401 ]]; then
        echo Authenicating to ${ADDRESS} with user ${USER}
        ATTEMPTS=1
        until [[ ${ATTEMPTS} -ge 5 ]]; do
            STATUS=$(curl -s --cookie-jar ${COOKIE} --config ${CONFIG} --user ${USER}:${PASSWORD} -o /dev/null -w "%{http_code}" ${ADDRESS}/data/projects)
            if [[ ${STATUS} == 401 ]]; then
                echo Attempt ${ATTEMPTS} failed to authenticate with given credentials. Waiting and trying again in 5 seconds.
                ATTEMPTS=$[${ATTEMPTS} + 1]
                sleep 5
            else
                break;
            fi
        done
    fi
                
    if [[ ${STATUS} == 200 ]]; then
        echo Authenticated, cookies stored in file ${COOKIE}
        echo --cookie ${COOKIE} >> ${CONFIG}
    elif [[ ${STATUS} == 401 ]]; then
        error -1 There was an error with your username ${USER} or password ${PASSWORD}. Return code: ${STATUS}.
    elif [[ ${STATUS} == 502 ]]; then
        error -1 Got status 502, which usually indicates that the targetted site is down or unreachable.
    else
        error -20 Got status ${STATUS}, which is weird. You should look into what\'s going on.
    fi
}

monitorTomcatLog() {
    echo monitorTomcatLog
    INDEX=0
    LOGFILE=/var/log/tomcat/catalina.$(date +%Y-%m-%d).log
    echo Catalina logfile ${LOGFILE}
    until [[ -e ${LOGFILE} || ${INDEX} > 5 ]]; do
        ((INDEX += 1))
        sleep 2
    done
    [[ -e ${LOGFILE} ]] && { return 0; } || { return 127; }
}

monitorTomcatStatus() {
    echo monitorTomcatStatus
    STATUS_MSG="^INFO: Server startup in"
    QUIET="--quiet"
    WAIT_FOR=180
    while [[ $1 ]]; do
        if [[ $1 =~ ^[0-9]+$ ]]; then
            WAIT_FOR=$1
        elif [[ $1 == "verbose" ]]; then
            QUIET=""
        else
            STATUS_MSG=$1
        fi
        shift
    done

    LOGFILE=/var/log/tomcat/catalina.$(date +%Y-%m-%d).log
    #timeout ${WAIT_FOR} sed ${QUIET} "/${STATUS_MSG}/ q" <(timeout $((WAIT_FOR + 5)) tail -n 0 -f $LOGFILE)
    timeout ${WAIT_FOR} sed ${QUIET} "/${STATUS_MSG}/ q" <(timeout $((WAIT_FOR + 5)) tail  -f $LOGFILE)
    STATUS=$?
    if [[ ${STATUS} == 0 ]]; then
        echo Found requested token in Tomcat startup log: "${STATUS_MSG}";
    else
        echo Did not find requested token in Tomcat startup log: "${STATUS_MSG}". The last lines in the log are:; tail -n 40 ${LOGFILE};
    fi
    return ${STATUS}
}

monitorTomcatStartup() {
    echo monitorTomcatStartup
    monitorTomcatLog $@
    STATUS=$?
    [[ ${STATUS} == 0 ]] || { echo No start-up log for Tomcat found at /var/log/tomcat. Status unknown, exiting without monitoring start-up progress.; return ${STATUS}; }
    monitorTomcatStatus $@
    return $?
}

waitForFile() {
    # The file name MUST be specified.
    if [[ -z ${1} ]]; then
        showHelp -1 You must specify the name of the file to monitor.
    elif [[ ${1} == -h ]]; then
        showHelp
    else
        FILENAME=${1}
    fi

    # Shift the args and see if there are optional wait times set.
    shift

    START=10
    COMPLETE=5
    WAIT=1
    DEBUG=false
    while [[ -n ${1} ]]; do
        if [[ ${1} == -s ]]; then
            [[ -z ${2} ]] && { showHelp -3 No value specified with the -s option, exiting.; }
            START=${2}
            shift; shift;
        elif [[ ${1} == -c ]]; then
            [[ -z ${2} ]] && { showHelp -4 No value specified with the -c option, exiting.; }
            COMPLETE=${2}
            shift; shift;
        elif [[ ${1} == -w ]]; then
            [[ -z ${2} ]] && { showHelp -5 No value specified with the -w option, exiting.; }
            WAIT=${2}
            shift; shift;
        elif [[ ${1} == -d ]]; then
            DEBUG=true
            shift;
        fi
    done

    COUNT=0

    while [[ ! -e ${FILENAME} && ${COUNT} -lt ${START} ]]; do
        sleep ${WAIT}
        debug ${FILENAME} does not yet exist on check ${COUNT}
        COUNT=$((COUNT+1))
    done

    [[ ! -e ${FILENAME} ]] && { error -2 The file ${FILENAME} did not appear within the timeout period.; }

    COUNT=0
    FILESIZE=$(stat -c%s ${FILENAME})
    debug ${FILENAME} found with initial size of ${FILESIZE}

    while [[ ${COUNT} -lt ${COMPLETE} ]]; do
        sleep ${WAIT}
        if [[ ${FILESIZE} == $(stat -c%s ${FILENAME}) ]]; then
            COUNT=$((COUNT+1))
            debug Size of ${FILENAME} is the same as check ${COUNT}
        else
            COUNT=0
            FILESIZE=$(stat -c%s ${FILENAME})
            debug Size of ${FILENAME} has changed to ${FILESIZE}, resetting count.
        fi
    done

    debug Size was the same ${COUNT} times, exiting cleanly.
}
