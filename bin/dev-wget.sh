#!/bin/bash
#
# dev-wget.sh
# XNAT http://www.xnat.org
# Copyright (c) 2020, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# This script retrieves the most recent development WAR file from ci.xnat.org.
#

URL="https://ci.xnat.org/job/XNAT%20Web/ws/build/libs/xnat-web.war"

wget -O xnat-web.war $URL
