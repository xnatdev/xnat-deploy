#!/bin/bash
#
# dev-reset.sh
# XNAT http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# This script is used to scrub a standard XNAT dev server in preparation for deployment of
# a new dev or test build.
#

ORIGIN=`dirname $0`
source ${ORIGIN}/macros.sh

function stop_tomcat() {
 echo stop tomcat
 [[ $(sudo -n systemctl is-active tomcat ) == *active* ]] && { echo Tomcat is running, shutting it down.; sudo -n systemctl stop tomcat; } || { echo Tomcat is not running.; }

 # Sleep for a couple seconds just to make sure it's done.
 sleep 2
}

function start_tomcat() {
 echo Starting Tomcat ...
 sudo -n systemctl start tomcat

 monitorTomcatStartup verbose
 STATUS=$?
 [[ ${STATUS} == 0 ]] && { echo Tomcat was started successfully.; } || { echo Tomcat does not appear to have started properly. Please check the application logs.; }
 exit ${STATUS}
}

function reset_database() {
 echo reset database
 # Check for database user and role.
 sudo -n -u postgres psql -c '\du' | cut -f 1 -d \| | grep -qw ${XNAT_USER}
 if [[ $? == 0 ]]; then
    echo User exists $XNAT_USER Check to see if user has DB create rights
    sudo -n -u postgres psql -c '\du' | fgrep ${XNAT_USER} | cut -f 2 -d \| | grep -qw "Create DB"
    if [[ $? != 0 ]]; then
        sudo -n -u postgres psql -c "ALTER USER ${XNAT_USER} WITH CREATEDB"
        echo Updated ${XNAT_USER} permissions to add CREATEDB
    else
        echo Database user ${XNAT_USER} exists and has the appropriate permissions.
    fi
 else
    echo Need to call createuser -d ${XNAT_USER}
    sudo -n -u postgres createuser -d ${XNAT_USER}
    echo Created new database user ${XNAT_USER}
 fi

 # Clear the existing database if it exists.
 sudo -n -u postgres psql -lqt | cut -d \| -f 1 | grep -qw ${XNAT_USER}
 if [[ $? == 0 ]]; then
    echo Dropping existing database ${XNAT_USER}...
    sudo -n -u postgres dropdb ${XNAT_USER}

    # If the database wasn't successfully dropped...
    if [[ $? != 0 ]]; then
        # Terminate all current connections to the database and try again.
        sudo -n -u postgres psql -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${XNAT_USER}'"
        sudo -n -u postgres dropdb ${XNAT_USER}
        [[ $? == 0 ]] && { echo Reset connections to database ${XNAT_USER} and successfully dropped it, proceeding...; } || { echo Failed to reset connection or delete data ${XNAT_USER}, something went wrong. Please check configuration.; exit 127; }
    else
        echo Dropped the existing database ${XNAT_USER}, proceeding...
    fi
 else
    echo Database ${XNAT_USER} does not exist, proceeding...
fi

 # Now create a new empty instance of the user database.
 echo Creating new empty database ${XNAT_USER}.
 sudo -n -u postgres createdb -O ${XNAT_USER} ${XNAT_USER}
}

function reset_tomcat() {
 echo reset tomcat

 # Archive and clear the logs.
 LOG_COUNT=$(find /var/log/tomcat -maxdepth 1 -type f | wc -l)
 if [[ ${LOG_COUNT} -gt 0 ]]; then
    LOG_ARCHIVE=/var/log/tomcat/archives/$(date +%Y%m%d%H%M%S)
    echo Making a log archive ${LOG_ARCHIVE}
    mkdir -p ${LOG_ARCHIVE}
    find /var/log/tomcat -mindepth 1 -maxdepth 1 -type f -exec mv '{}' ${LOG_ARCHIVE} \;
    echo Archived existing logs to ${LOG_ARCHIVE}.
 else
    echo No log files found in the Tomcat log folder, no archive created.
 fi

 echo Removing any web applications.
 rm -rf /usr/share/tomcat/webapps/*
}


function reset_application() {
 echo reset application
 echo Clearing archives and other artifacts...
 XNAT_HOME=$(eval echo ~${XNAT_USER})
 DATA_ROOT=$(dirname ${XNAT_HOME})
 ARCHIVE_LOGS=${XNAT_HOME}/.archives/$(date +%Y%m%d%H%M%S)

 # Archive the existing logs files.
 echo Moving logs from ${XNAT_HOME}/logs to ${ARCHIVE_LOGS}/xnat
 mkdir -p ${ARCHIVE_LOGS}/xnat
 mkdir -p ${XNAT_HOME}/logs
 find ${XNAT_HOME}/logs -maxdepth 1 -type f -exec mv '{}' ${ARCHIVE_LOGS}/xnat \;
 echo Moving logs from ${DATA_ROOT}/pipeline/logs to ${ARCHIVE_LOGS}/pipeline
 mkdir -p ${ARCHIVE_LOGS}/pipeline
 mkdir -p ${DATA_ROOT}/pipeline/logs
 find ${DATA_ROOT}/pipeline/logs -maxdepth 1 -type f -exec mv '{}' ${ARCHIVE_LOGS}/pipeline \;

 # Now clear temp and data archives.
 echo Clearing ${XNAT_HOME}/work
 rm -rf ${XNAT_HOME}/work/*
 echo Clearing ${DATA_ROOT}/archive
 rm -rf ${DATA_ROOT}/archive/*
 echo Clearing ${DATA_ROOT}/build
 rm -rf ${DATA_ROOT}/build/*
 echo Clearing ${DATA_ROOT}/cache
 rm -rf ${DATA_ROOT}/cache/*
 echo Clearing ${DATA_ROOT}/ftp
 rm -rf ${DATA_ROOT}/ftp/*
 echo Clearing ${DATA_ROOT}/prearchive
 rm -rf ${DATA_ROOT}/prearchive/*

 # Clean up any left-over war files.
 echo Cleaning up old war files
 touch ~/tmp.war
 rm ~/*.war
}

# Test for user name, default to xnat.
# This is the user name for postgres accounts
[[ -z "${1}" ]] && { XNAT_USER=xnatdev; } || { XNAT_USER=${1}; }

stop_tomcat
reset_database
reset_tomcat
reset_application
start_tomcat

